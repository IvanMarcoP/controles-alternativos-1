using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VibrarBoton : MonoBehaviour
{
    [SerializeField] private Button botonVibracion;


    private void OnEnable() 
    {
        botonVibracion.onClick.AddListener(VibrarTelefono);
    }

    private void OnDisable()
    {
        botonVibracion.onClick.RemoveListener(VibrarTelefono);
    }

    public void VibrarTelefono()
    {
        Debug.Log("Vibrando...");
        Handheld.Vibrate();
    }
}
